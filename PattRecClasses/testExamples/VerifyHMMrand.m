q=[0.75; 0.25]; %Initial probability matrix q
A=[0.99 0.01; 0.03 0.97]; %Transition matrix A
mc=MarkovChain(q, A); %State generator

g1=GaussD('Mean',0,'StDev',1); %Distribution for state=1
g2=GaussD('Mean',3,'StDev',2); %Distribution for state=2

h=HMM(mc, [g1; g2]); %The HMM

S=rand(mc,10000); %Generate a sequence of T=10000 state integer numbers

stateFrequencies=[sum(S==1)/10000; sum(S==2)/10000] %Calculate the relative frequency of occurences of S=1 and S=2.

x=rand(h,10000)
[mean(x), var(x)]
% plot([1:1:10000], x);
% title('500 samples X_t as a function of t');
% xlabel('Frame t');
% ylabel('Observation X');